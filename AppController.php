<?php
namespace app\controllers;
use Yii;
class DDoS
{
    private $time_block; // время блокировки,сек
    private $time_token; // контролируемый период, сек
    private $count_request; // количестве запросов


    public function __construct($time_block,$time_token, $count_request)
    {
        $this->time_block = $time_block;
        $this->time_token = $time_token;
        $this->count_request  = $count_request;
    }

    public function is_block($ip)
    {
        $now=time();
        $sql = "SELECT * from ddos WHERE ip=:ip";
        $data=Yii::$app->db->createCommand($sql,['ip'=>$ip])->queryOne();
        if ($data) { // имеется запись
            if ($data['block_time']>0) {
                if ($data['block_time']<$now) { // можно снимать с блокировки
                    $this->new_token($ip);
                    return false;
                }
                else
                    return ($data['block_time']-$now);
            }
            if ($data['token_time']<$now) { // пришло время выдать новый токен
                $this->new_token($ip);
                return false;
            }
            if ($data['request_limit']<=1) { // превышен лимит - блокируем
                $this->set_block($ip);
                return true;
            }
            else // уменьшаем количество разрешенных запросов
                $this->dec_limit($ip,$data['request_limit']);
        }
        else // запросов не было, создаем новыю запись
            $this->new_ip($ip);
    }

    public function dec_limit($ip,$current){
        $params = [
            'request_limit' =>($current-1),
        ];
        Yii::$app->db->createCommand()->update('ddos', $params, ['ip'=>$ip])->execute();
    }

    public function new_ip($ip){
        $params = [
            'ip' =>$ip,
            'token_time' => time()+$this->time_token,
            'request_limit'=>$this->count_request,
            'block_time'=>0
        ];
        Yii::$app->db->createCommand()->insert('ddos', $params)->execute();
    }

    public function set_block($ip){
        $params = [
            'block_time' =>time()+$this->time_block,
        ];
        Yii::$app->db->createCommand()->update('ddos', $params, ['ip'=>$ip])->execute();
    }

    public function new_token($ip){
        $params = [
            'token_time' => time()+$this->time_token,
            'request_limit'=>$this->count_request,
            'block_time'=>0
        ];
        Yii::$app->db->createCommand()->update('ddos', $params, ['ip'=>$ip])->execute();
    }

}

/**
 * блокировка запросов при превышении количества разрешенных запросов tokens.
 * параметры:
 * new DDoS(время блокирования в секундах, анализируемый перод в секундах, количество запросов в период)
 */

$check= new DDoS(600,60,5);// время блокирования, анализируемый перод, количество запросов в период
$block=$check->is_block($_SERVER['REMOTE_ADDR']);
if ($block) {
    http_response_code(429);
    header('Retry-After: '.$block);

}
else {
   header('Content-Type: text/html');
    echo "Hello world!";
}
exit();


